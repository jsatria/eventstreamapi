package com.forecastthis.eventstream.db.api;

import com.forecastthis.eventstream.db.dao.Event;
import com.forecastthis.eventstream.db.dao.Location;
import com.forecastthis.eventstream.db.dao.Source;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Properties;

public class Datasource {
	private static SessionFactory factory;

	static Logger logger = LoggerFactory.getLogger(Datasource.class);

	public static SessionFactory getSessionFactory(){
		if (factory == null){
			logger.info("Creating event stream data source pool");
			try{
				// Create the SessionFactory from hibernate.properties
				// load properties
				Properties prop = new Properties();
				prop.load(new FileInputStream("/srv/config/eventstream.postgres.api.properties"));

				factory= new Configuration()
						.setProperties(prop)
						.addAnnotatedClass(Event.class)
						.addAnnotatedClass(Location.class)
						.addAnnotatedClass(Source.class)
						.buildSessionFactory();
			}catch (Exception e) {
				logger.error("Failed to create sessionFactory object.", e);
			}
		}

		return factory;
	}
}
