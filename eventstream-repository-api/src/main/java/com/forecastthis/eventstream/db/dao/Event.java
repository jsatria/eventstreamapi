package com.forecastthis.eventstream.db.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by jonathan on 19/09/16.
 */
@Entity
@Table(name = "event")
public class Event {
	@Getter @Setter @Id private Long id;
	@Getter @Setter private String name;
	@Getter @Setter private String alternative_name;
	@Getter @Setter private Date start_date;
	@Getter @Setter private Date end_date;
	@Getter @Setter private Date last_modified_date;
	@Getter @Setter private String description;
	@Getter @Setter private Integer attendance;
	@Getter @Setter private Integer last_attendance;
	@Getter @Setter private String organization_contact_details;
	@Getter @Setter private String fee;
	@Getter @Setter private String source_keywords;
	@Getter @Setter private String url;
	@Getter @Setter private String frequency;

	@Getter @Setter @OneToOne private Location location;
	@Getter @Setter @OneToOne private Source source;

}
