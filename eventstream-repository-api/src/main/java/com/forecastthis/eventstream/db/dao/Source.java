package com.forecastthis.eventstream.db.dao;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by jonathan on 21/09/16.
 */
@Entity
@Embeddable
public class Source {
	@Getter @Setter @Id private Long id;
	@Getter @Setter private String name;
	@Getter @Setter private String url;
}
