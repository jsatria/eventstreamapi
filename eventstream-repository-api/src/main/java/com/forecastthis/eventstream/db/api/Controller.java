package com.forecastthis.eventstream.db.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.forecastthis.eventstream.db.dao.Event;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Route;

import java.util.List;

/**
 * Created by jonathan on 19/09/16.
 */
public class Controller {
	static Logger logger = LoggerFactory.getLogger(Controller.class);

	// a list of endpoints that reflect common queries made for the event stream project

	public static Route getEventsWithinDistance = (request, response) -> {
		logger.info("API: " + request.url());
		String latitude = request.params("latitude");
		String longitude = request.params("longitude");
		Double distance = Double.parseDouble(request.params("distance"));

		String query = "SELECT * FROM Event WHERE location_id IN (SELECT id FROM location WHERE ST_DWithin(point, ST_MakePoint("
				+ longitude+ "," + latitude + "), " + distance + ")) ";

		logger.info("Query: " + query);
		Session s = null;
		try{
			s  = Datasource.getSessionFactory().openSession();
			List events = s.createNativeQuery(query).addEntity(Event.class).list();

			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(events);

		} catch (Exception e){
			logger.error("Error when trying to establish connection to source", e);
		} finally{
			s.close();
		}
		return null;
	};
}
