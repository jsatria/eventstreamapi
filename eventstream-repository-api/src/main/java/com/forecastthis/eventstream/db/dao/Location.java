package com.forecastthis.eventstream.db.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by jonathan on 20/09/16.
 */
@Entity
@Embeddable
public class Location {
	@Getter @Setter @Id	private Long id;
	@Getter @Setter private String name;
	@Getter @Setter private String alternative_name_1;
	@Getter @Setter private String alternative_name_2;
	@Getter @Setter private Long location_type_id;
	@Getter @Setter private String country;
	@Getter @Setter private String state;
	@Getter @Setter private String city;
	@Getter @Setter private String street_address;
	@Getter @Setter private String zip_code;
	@Getter @Setter private Double latitude;
	@Getter @Setter private Double longitude;
	@Getter @Setter private String floor_size;
	@Getter @Setter private Integer max_capacity;
}
